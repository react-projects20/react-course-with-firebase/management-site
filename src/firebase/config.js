import firebase from "firebase";
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyAR5ILX8Ol5Izlgo51JMQYQHs6PZXbrGP8",
    authDomain: "projectmanagementsite-e4990.firebaseapp.com",
    projectId: "projectmanagementsite-e4990",
    storageBucket: "projectmanagementsite-e4990.appspot.com",
    messagingSenderId: "241218029598",
    appId: "1:241218029598:web:411a8be77481e16784c60f"
  };

  //init firebase
  firebase.initializeApp(firebaseConfig)

  //init services
  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()
  const projectStorage = firebase.storage()

  //timestamp
  const timestamp = firebase.firestore.Timestamp

  export { projectFirestore, projectAuth, projectStorage, timestamp }