//Styles
import './Avatar.css'

export default function ({src}) {
    return (
        <div className='avatar'>
            <img src={src} alt='user avatar'/>
        </div>
    )
}